var isbn;

function loadData() {
    var prGetData = new Promise(function (resolve, reject) {
        var id = sessionStorage.getItem('resID');
        $.ajax({
            method: 'GET',
            // url: 'modules/shop/ctrl_shop.php?op=getItem&aux=' + id,
            url: 'shop/getItem/' + id,
            datatype: "json"
        })

            .done(function (data) {
                console.log(data);
                data = JSON.parse(data);
                // console.log(data);

                $('#title').append(data[0].nameResource);
                $('#editorial').append(data[0].editorialResource);
                $('#release').append(data[0].releaseResource);

                isbn = data[0].ISBNResource;
                resolve();
            });

    });

    prGetData
    .then(
        function() {
            getRelateds()
        }
    );


}


function getRelateds() {
    // console.log('ISBN ' + isbn);
    var url = "https://www.googleapis.com/books/v1/volumes?q=" + isbn;
    console.log(url);
    
    $.ajax({
        method: 'GET',
        url: url,
        datatype: 'json'
    })
        .done(function (data) {
            console.log(data);

            $("#img-book").attr("src", data['items'][0]['volumeInfo']['imageLinks']['thumbnail']);
            $('#img-book').removeAttr('hidden');
            $('#loading-api-search').attr('hidden', true);
            
            for (var i = 1; i < 4; i++) {
                var show = "";
                console.log(data['items'][i]);
                // console.log(data['items'][i]['volumeInfo']['imageLinks']['smallThumbnail']);
                var name = data['items'][i]['volumeInfo']['title'];  
                var img = data['items'][i]['volumeInfo']['imageLinks']['thumbnail'];
                var redirect = data['items'][i]['volumeInfo']['previewLink'];

                // show += '<div class="card col-sm-2">'
                show += '<img src="' + img + '">'
                show += '<h5>'+ name +'</h5>'
                show += '<a target="_blank" href="' + redirect + '" class="btn btn-outline-dark">Show more...</a>'
                show += '<hr>'
                // show += '</div>'

                $('#col-related').append(show);
                
            }
        })

        .fail(function () {
            setTimeout(function() { // REINTENTAR SI LA API FALLA 
                getRelateds();
            }, 10000);
        })
}



$(document).ready(function () {
    if (sessionStorage.getItem('resID') == null) {
        window.location.href = "./";
    } else {
        loadData();
    }
})
