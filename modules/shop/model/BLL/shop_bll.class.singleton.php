<?php
    class shop_bll {
        private $dao;
        private $db;

        static $_instance;

        private function __construct() {
            $this->dao = shop_dao::getInstance();
            $this->db = Db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self)){
                self::$_instance = new self();
            }
            return self::$_instance;
        }
 
        private function getActiveUserID() {
            if (!isset($_SESSION['activeUser'])) {
                throw new Exception('User not logged');
            }
            $user = unserialize($_SESSION['activeUser']);
            return $user->getID();
        }

        private function executeDAO($functionName, $arrAguments = '') {
            if (method_exists($this->dao, $functionName)) {
                return $this->dao->$functionName($this->db, $arrAguments);
            } else {
                throw new Exception('Not exists DAO function -> '. $functionName);
            }
        }

        /////////GET FUNCTIONS//////////////

        public function item($arrAguments) {
            return $this->executeDAO('daoGETitem');
        }

        /////////////POST FUNCTIONS//////////////////////


    }
    