<div class="container" id="cont">
    <div class="row">
        <div class="col">
            <h1 id="title"></h1>
            <h6 id="release"></h6>
            <h6 id="editorial"></h6>
        </div>

        <div class="col-sm-2" id="col-related">
            <span class="spinner-border spinner-border" id="loading-api-search" role="status" aria-hidden="true"></span>
            <img src="" id="img-book" hidden>
            <hr>

        </div>

    </div>
    <div class="row">
        <div class="col"></div>
        <div class="col-sm-2" id="col-related"></div>
    </div>

</div>