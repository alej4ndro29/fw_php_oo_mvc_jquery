<?php

    if(!isset($_SESSION)){
        session_start();
    }


    if(!isset($_GET['op'])){
        include 'view/cartDetails.php';
    } else {
        include 'dao/DAOitems.php';
        include __DIR__."/../../components/login/user.class.php";
        switch ($_GET['op']) {
            case 'addToCart':
                // echo 'ok';
                try {
                    $result = searchItem($_POST['item']);

                    if ($result->num_rows != 1){
                        throw new Exception('error-item');
                    }

                    $result = mysqli_fetch_assoc($result);
                    
                    $ok = true;
                    for ($i=0; $i < count($_SESSION['cart']); $i++) { 
                        if ($_SESSION['cart'][$i]['productID'] == $_POST['item']) {
                            $_SESSION['cart'][$i]['quantity']++;

                            if(isset($_SESSION['activeUser'])) {
                                $user = unserialize($_SESSION['activeUser']);
                                addOne($user->getID(), 
                                        $_SESSION['cart'][$i]['productID']);
                            }

                            $ok = false;
                        }
                    }

                    if ($ok) {
                        array_push($_SESSION['cart'], array(
                            'productID' =>  $result['IDResource'],
                            'quantity' => 1
                        ));

                        if(isset($_SESSION['activeUser'])) {
                                $user = unserialize($_SESSION['activeUser']);
                                createCart($user->getID(), 
                                            $_POST['item'],
                                            1);
                        }

                    }

                    print_r($_SESSION['cart']);

                } catch (Exception $e) {
                    echo $e->getMessage();
                }

                break;
            
            case 'getList':
                echo json_encode($_SESSION['cart']);
                break;

            case 'getElementList':
                // echo ($_GET['item']);
                $info = array();
                $result = getNamePrice($_GET['item']);
                $result = mysqli_fetch_assoc($result);

                $quantity;
                for ($i=0; $i < count($_SESSION['cart']); $i++) { 
                    if ($_SESSION['cart'][$i]['productID'] == $_GET['item']) {
                        // echo ' dentro if ';
                        $quantity = $_SESSION['cart'][$i]['quantity'];                        
                    }
                }

                $info['name'] = $result['nameResource'];
                $info['price'] = $result['price'];
                $info['quantity'] = $quantity;
                echo json_encode($info);
                break;

            case 'cartDestroy':
                $_SESSION['cart'] = null;
                $_SESSION['cart'] = array();

                if(isset($_SESSION['activeUser'])) {
                    $user = unserialize($_SESSION['activeUser']);
                    deleteCart($user->getID());
                }

                break;
            case 'removeItem':
                $ok=false;
                for ($i=0; $i < count($_SESSION['cart']); $i++) { 
                    if ($_SESSION['cart'][$i]['productID'] == $_POST['item']) {
                        unset($_SESSION['cart'][$i]);
                        $_SESSION['cart']=array_values($_SESSION['cart']);

                        if(isset($_SESSION['activeUser'])) {
                            $user = unserialize($_SESSION['activeUser']);
                            removeItem($user->getID(), 
                                        $_POST['item']);
                        }

                        $ok=true;
                    }
                }
                echo $ok;
                break;
            case 'addOne':
                $ok=false;
                for ($i=0; $i < count($_SESSION['cart']); $i++) { 
                    if ($_SESSION['cart'][$i]['productID'] == $_POST['item']) {
                        $_SESSION['cart'][$i]['quantity']++;
                        $ok=true;
                        if(isset($_SESSION['activeUser'])) {
                            $user = unserialize($_SESSION['activeUser']);
                            addOne($user->getID(),
                                     $_POST['item']);
                        }
                    }
                }
                echo $ok;
                break;
            case 'minusOne':
                $ok=false;
                for ($i=0; $i < count($_SESSION['cart']); $i++) { 
                    if ($_SESSION['cart'][$i]['productID'] == $_POST['item']) {
                        if ($_SESSION['cart'][$i]['quantity'] > 1) {
                            $_SESSION['cart'][$i]['quantity']--;
                            
                            if(isset($_SESSION['activeUser'])) {
                                $user = unserialize($_SESSION['activeUser']);
                                minusOne($user->getID(), 
                                        $_POST['item']);
                            }

                        } else { 
                            // ELIMINAR EL PRODUCTO SI SOLO HAY UN ELEMENTO
                            unset($_SESSION['cart'][$i]);
                            $_SESSION['cart']=array_values($_SESSION['cart']);
                            if(isset($_SESSION['activeUser'])) {
                                $user = unserialize($_SESSION['activeUser']);
                                removeItem($user->getID(), 
                                            $_POST['item']);
                            }
                        }
                        $ok=true;
                    }
                }
                echo $ok;
                break;
            case 'completeOrder':
                try {
                    if (!isset($_SESSION['activeUser'])) {
                        throw new Exception('noUser');
                    }

                    if (count($_SESSION['cart']) == 0) {
                        throw new Exception('emptyCart');
                    }

                    $_SESSION['cart'] = null;
                    $_SESSION['cart'] = array();

                    $user = unserialize($_SESSION['activeUser']);
                    toHistory($user->getID());
                    deleteCart($user->getID());

                    echo 'completedOrder';
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                break;
            default:
                die;
                break;
        }

    }