<?php
    class cart_dao {
        static $_instance;

        private function __construct() {
        }

        public static function getInstance() {
            if(!(self::$_instance instanceof self)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        public function searchItem($db, $item) {
            $sql = "SELECT *
                    FROM Resources
                    WHERE IDResource = $item;";

            return $db->createQuery($sql);
        }

        public function searchAnItem($db, $item) {
            $sql = "SELECT *
                        FROM Resources
                        WHERE IDResource = $item;";

            $result = $db->createQuery($sql);
            return $db->listOne($result);
        }

        public function getItemsCondition($db, $data) {
            $condition = $data['condition'];

            $sql = "SELECT idResource, price
                    FROM Resources
                    WHERE IDResource IN $condition
                    ORDER BY idResource DESC;";

            $result = $db->createQuery($sql);
            return $db->listQuery($result);

        }

        public function toHistory($db, $data) {
            $sql = "INSERT INTO historyShop(
                        userID,
                        resourceID,
                        price,
                        quantity,
                        purchaseID
                    ) VALUES ";

            $insert = '';

            foreach ($data as $key) {
                if ($insert != '') {
                    $insert .= ',';
                }
                $insert .= "('".$key['userID']."','".$key['resourceID']."','".$key['price']."','".$key['quantity']."','".$key['purchaseID']."')";
            }
            $insert .= ';';
            $sql .= $insert;
            
            $db->createQuery($sql);
        }

    }
    
