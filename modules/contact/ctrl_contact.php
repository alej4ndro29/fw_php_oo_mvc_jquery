<?php
    class contact {
    
        const PATH_MODEL = __DIR__ . '/model/model/';
        static $_instance;

        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        private function loadHead() {
            include VIEW_INC_PATH . '/headers/topPageContact.php';
            include VIEW_INC_PATH . '/header.php';
        }

        private function loadFooter() {
            include VIEW_INC_PATH . '/footer.php';
            include VIEW_INC_PATH . '/bottom.php';
        }

        public function loadView() {
            $this->loadHead();
            include 'view/index.html';
            $this->loadFooter();
        }

        public function sendMail() {
            try {
                // Send mail to admin
                $from      = $_POST['name'];
                $userEmail = $_POST['email'];

                $to      = $_SESSION['ADMIN_EMAIL'];
                $subject = 'New message from <' . $userEmail . '>';
                $html    = $_POST['message'];

                sendMail($from, $to, $subject, $html);

                // Send mail to user
                $to      = $userEmail;
                $subject = 'Copy of your message.';
                sendMail($from, $to, $subject, $html);
            } catch (Exception $e) {
                header('HTTP/1.0 400 Bad error');
            }
        }

    }
