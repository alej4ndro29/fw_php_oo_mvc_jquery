var map;
var ubi = { lat: 38.8198521, lng: -0.6076066 };
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 38.8198521, lng: -0.6076066 },
        zoom: 15
    });
    var contentString = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<h1 id="firstHeading" class="firstHeading">Lorem Ipsum</h1>' +
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' +
        'Maecenas id placerat ex, vel placerat risus. Suspendisse ut tincidunt justo. Curabitur quis nisi quam. Mauris id convallis quam. Nam sit amet laoreet tellus.' +
        'Pellentesque consequat ante mollis bibendum ultricies. Donec non enim est. Nunc tristique mauris sapien, vel molestie lacus efficitur sit amet.' + 
        ' Praesent eget ex auctor, cursus tellus ac, sollicitudin felis.' +
        'Sed ultricies felis mollis risus venenatis tempor. Donec quis est posuere, consectetur neque vel, hendrerit est. ' + 
        'Pellentesque suscipit luctus leo, vitae euismod augue eleifend et. Quisque fringilla lacinia sem, et aliquet diam posuere eget.'
        '</div>' +
        '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    var marker = new google.maps.Marker({
        position: ubi,
        map: map,
        title: 'Uluru (Ayers Rock)'
    });
    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });

}


