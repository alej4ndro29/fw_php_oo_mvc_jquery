var webAuth;
var userProfile;
var accessToken;

function regVerifyNick() {
    var ok = false;
    var nick = $('#reg-nick').val();

    var regex = /^[a-zA-Z0-9]+$/;
    if (!regex.test(nick)) {
        $('#reg-nick').addClass('is-invalid');
    } else {
        $('#reg-nick').removeClass('is-invalid');
        ok = true;
    }
    return ok;
}

function regVerifyEmail() {
    var ok = false;
    var email = $('#reg-email').val();

    var regexp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!regexp.test(email)) {
        $('#reg-email').addClass('is-invalid');
    } else {
        $('#reg-email').removeClass('is-invalid');
        ok = true;
    }

    return ok;
}

function regVerifyPasswd() {
    var ok = false;
    var pass0 = $('#reg-password').val();
    var pass1 = $('#reg-password-rep').val();
    if (pass0 == pass1 && pass0 != "") {
        $('#reg-password').removeClass('is-invalid');
        $('#reg-password-rep').removeClass('is-invalid');
        ok = true;
    } else {
        $('#reg-password').addClass('is-invalid');
        $('#reg-password-rep').addClass('is-invalid');
    }
    return ok;
}

function registerActions() {
    var nickOK = regVerifyNick();
    var emailOK = regVerifyEmail();
    var passwdOK = regVerifyPasswd();

    if (nickOK && emailOK && passwdOK) {
        regSendData();
    }
}

function regSendData() {
    var data = $('#reg-form').serialize();
    $('#modal-err-content').empty();

    $('#reg-btn').attr("disabled", true);
    
    $.ajax({
        method: 'POST',
        url: 'login/register',
        datatype: 'json',
        data: data
    })
    
    .done(function (data) {
        console.log(data);
        data = JSON.parse(data);

        if (!data['correct']) {
            $('#modal-err-content').append(data['message']);
            $('#modal-error').modal('show');
            $('#reg-btn').attr("disabled", false);
        } else {
            $('#modal-err-content').append("Please check your email.");
            $('#modal-error').modal('show');
            $('#reg-btn').attr("disabled", false);
        }

    })

    .error(function (data) {
        $('#modal-err-content').append(data['statusText']);
        $('#modal-error').modal('show');
        $('#reg-btn').attr("disabled", false);
    });

}

function logVerifyNick() {
    var ok = false;
    var nick = $('#lg-nick').val();

    var regex = /^[a-zA-Z0-9]+$/;
    if (!regex.test(nick)) {
        $('#lg-nick').addClass('is-invalid');
    } else {
        $('#lg-nick').removeClass('is-invalid');
        ok = true;
    }
    return ok;
}

function logVerifyPasswd() {
    var ok = false;
    var pass = $('#lg-password').val();

    if (pass == "") {
        $('#lg-password').addClass('is-invalid');
    } else {
        $('#lg-password').removeClass('is-invalid');
        ok = true;
    }
    return ok;
}

function logSendData() {
    var data = $('#lg-form').serialize();
    $('#modal-err-content').empty();

    $.ajax({
        method: 'POST',
        url: amigableUrl('login/login'),
        datatype: 'json',
        data: data
    })

    .done(function (data) {
        console.log(data);
        data = JSON.parse(data);

        if(data['correct']) {
            setUserToken(data['token']);
            window.location.href = amigableUrl('homePage');
        }
    })
    
    .error(function (data) {
        $('#modal-err-content').append(data['statusText']);
        $('#modal-error').modal('show');
        $('#reg-btn').attr("disabled", false);
    });
}

function loginActions() {
    var nickOK = logVerifyNick();
    var passwdOK = logVerifyPasswd();

    if (nickOK && passwdOK) {
        logSendData();
    }
}

function inicialiceWebAuth() {
    webAuth = new auth0.WebAuth({
        domain: 'alej4-dev.eu.auth0.com',
        clientID: 'HPa7flJZeZuk99N2ycB9jDC6AzCpTOlb',
        redirectUri: window.location.href,
        audience: 'https://' + 'alej4-dev.eu.auth0.com' + '/userinfo',
        responseType: 'token id_token',
        scope: 'openid profile',
        leeway: 60
    });
}

function handleAuthentication() {
    webAuth.parseHash(function (err, authResult) {
        if (authResult && authResult.accessToken && authResult.idToken) {
            sendSocialLoginUser(authResult);
        }
    });
}

function socialLogin() {
    webAuth.authorize();
}

function sendSocialLoginUser(user) {
    localStorage.setItem('socialLogin', true);
    $.ajax({
        method: 'POST',
        url: '/login/socialLogin',
        datatype: 'json',
        data: user
    })
    .done(function (data) {
        data = JSON.parse(data);
        setUserToken(data['userToken']);
        window.location.href = amigableUrl('homePage');
    })
    .error(function (data) {
        $('#modal-err-content').append(data['statusText']);
        $('#modal-error').modal('show');
        $('#reg-btn').attr("disabled", false);
    });
}

function socialLogout() {
    localStorage.removeItem('socialLogin');
    webAuth.logout({
        returnTo: window.location.href,
        client_id: 'HPa7flJZeZuk99N2ycB9jDC6AzCpTOlb'
    });
}

function recPasswd() {
    var email = $('#inp-rec-passwd').val();

    $.ajax({
        method: 'POST',
        url: '/login/recPasswd',
        datatype: 'json',
        data: {email: email}
    })
    .done(function (data) {
        console.log(data);
        alert('Check your mail');
    });

}

function setRecPasswd() {
    var pass0 = $('#passwd').val();
    var pass1 = $('#passwd1').val();

    var url = window.location.href;
    var split = url.split('/');
    var token = split[split.length-1];

    if (pass0 == pass1 && pass0 != '' ) {
        $.ajax({
            method: 'POST',
            url: '/login/setRecPasswd',
            datatype: 'json',
            data: { passwd: pass0, token: token }
        })
        .done(function (data) {
            console.log(data);
            window.location.href = amigableUrl('login');
        })
    } else {
        alert('Invalid password');
    }

}

$(document).ready(function name(params) {

    inicialiceWebAuth();
    handleAuthentication();

    if(getUserToken() != null) {
        window.location.href = amigableUrl('');
    }

    if(localStorage.getItem('socialLogin')) {
        socialLogout();
    }

    // Login actions
    $('#lg-nick').on("keyup", function (e) {
        if (e.keyCode == 13) {
            document.getElementById('lg-password').select();
            
        }
    });

    $('#lg-password').on("keypress", function (e) {
        if (e.keyCode == 13) {
            loginActions();
        }
    });

    $('#lg-btn').on('click', function () {
        loginActions();
    });
    
    $('#social-login').on('click', function () {
        socialLogin();
    });

    // Register actions
    $('#reg-nick').on("keyup", function (e) {
        if (e.keyCode == 13) {
            document.getElementById('reg-email').select();
        }
    });

    $('#reg-email').on("keyup", function (e) {
        if (e.keyCode == 13) {
            document.getElementById('reg-password').select();
        }
    });

    $('#reg-password').on("keyup", function (e) {
        if (e.keyCode == 13) {
            document.getElementById('reg-password-rep').select();
        }
    });

    $('#reg-password-rep').on("keypress", function (e) {
        var regBtn = document.getElementById('reg-btn');
        var regBtnIsEnabled = (regBtn.getAttribute('disabled') != 'disabled');
        if (e.keyCode == 13 && regBtnIsEnabled) {
            registerActions();
        }
    });

    $('#reg-btn').on('click', function () {
        registerActions();
    });

    $('#lf-recpassword').on('click', function () {
        $('#modal-recover').modal('show');    
    });

    $('#btn-rec-passwd').on('click', function () {
        recPasswd();
    });

    $('#set-rec-passwd').on('click', function () {
        setRecPasswd();
    });
    
});