<?php
    class order_dao {
        static $_instance;

        private function __construct() {
        }

        public static function getInstance() {
            if(!(self::$_instance instanceof self)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        public function daoGetData($db) {
            $sql = "SELECT IDResource ,nameResource, typeResource, ISBNResource, COUNT(resourceID) likes
                    FROM Resources r
                    LEFT JOIN likeRes l ON r.IDResource = l.resourceID
                    GROUP BY IDResource;";

            $result = $db->createQuery($sql);
            
            return $result;
        }


        public function isLiked($db, $data) {
            $idUser = $data['userId'];
            $idResource = $data['idResource'];

            $sql = "SELECT count(*) count
                    FROM likeRes
                    WHERE userID = '$idUser'
                    AND resourceID = '$idResource';";
            
            $result = $db->createQuery($sql);
            $result = $db->listOne($result);

            return $result['count'];
        }

        public function addLike($db, $data) {
            $idUser = $data['userId'];
            $idResource = $data['idResource'];
            
            $sql = "INSERT INTO likeRes(
                        userID,
                        resourceID
                    )
                    VALUES(
                        '$idUser',
                        '$idResource'
                    );";
            
            $db->createQuery($sql);
        }

        public function removeLike($db, $data) {
            $idUser = $data['userId'];
            $idResource = $data['idResource'];

            $sql = "DELETE FROM likeRes
                    WHERE userID = '$idUser'
                    AND resourceID = '$idResource';";

            $db->createQuery($sql);
        }

    }
    
