<?php
    class order_bll {
        private $dao;
        private $db;

        static $_instance;

        private function __construct() {
            $this->dao = order_dao::getInstance();
            $this->db = Db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self)){
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        private function executeDAO($functionName, $arrAguments = '') {
            if (method_exists($this->dao, $functionName)) {
                return $this->dao->$functionName($this->db, $arrAguments);
            } else {
                throw new Exception('Not exists DAO function -> '. $functionName);
            }
        }

        /////////GET FUNCTIONS//////////////

        public function getData() {
            return $this->executeDAO('daoGetData');
        }

        /////////////POST FUNCTIONS//////////////////////

        public function newLike ($data) {
            $userToken = $data['userToken'];

            $activeUser = User::getUserByToken($userToken);
            
            if($activeUser == null) {
                throw new Exception('Error');                
            }

            $info = array(
                'userId' => $activeUser['id'],
                'idResource' => $data['idResource']
            );

            $isLiked = $this->executeDAO('isLiked', $info);
            
            if($isLiked) {
                $this->executeDAO('removeLike', $info);
                $response['message'] = 'removedLike';
            } else {
                $this->executeDAO('addLike', $info);
                $response['message'] = 'addedLike';
            }

            $newToken = User::updateAppToken($userToken);
            $response['newToken'] = $newToken;

            return $response;
        }

    }
    