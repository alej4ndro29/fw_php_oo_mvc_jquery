<?php
    //include __DIR__.'/../../components/login/user.class.php';

    if (!isset($_SESSION)) {
        session_start();
        include __DIR__.'/../../components/login/user.class.php';
    }

    if (!isset($_SESSION['activeUser'])) { // NO LOGUEADO
        die('<script>window.location.href="index.php?page=login";</script>');
    } else { // LOGUEADO NO ADMIN
        $user = unserialize($_SESSION['activeUser']);
        if (!$user->isAdmin()) {
            echo '403 forbidden';
            die;
        }
    }

    include 'subCtrl/subCtrl_generateList.php'; // FUNCTION
    //debug($_GET);

    if(!isset($_GET['op'])) {
        //echo 'Listar elementos';
        include 'view/listResources.php';
    } else {
        switch ($_GET['op']) {
            case 'create':
                // echo 'create';
                include 'subCtrl/subCtrl_newResource.php';
                break;

            case 'read':
                //echo 'read';
                include 'view/readResources.php';
                break;

            case 'readModal':
                // CALCULAR TIEMPO DE INACTIVIDAD
                $interval = $_SESSION['activityTime']->diff(new DateTime('now'));
                $minutesInactivity = ($interval->days * 24 * 60) + ($interval->h * 60) + $interval->i;

                if ($minutesInactivity > 5) {
                    $_SESSION['activeUser'] = null;
                    $_SESSION['activityTime'] = null;
                    session_destroy();
                } else {
                    $_SESSION['activityTime'] = new DateTime('now');
                }

                include 'dao/DAOread.php';
                $res = DAOreadmodal();
                foreach ($res as $key) {
                    echo json_encode($key);
                }
                break;

            case 'update';
                //echo 'update';
                include 'subCtrl/subCtrl_updateResource.php';
                break;

            case 'delete':
                include 'subCtrl/subCtrl_deleteResources.php';
                break;
    
            default:
                die('<script>window.location.href="index.php?page=resources";</script>');
        }

    }
