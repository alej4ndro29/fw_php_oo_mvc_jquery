$(document).ready(function () {
    $('#listTable').on('click', '.readRec',function name() {
        // console.log(this.getAttribute('id'));
        var id = this.getAttribute('id');


        $.ajax({
            method: "GET",
            url: "modules/resources/ctrl_resources.php?page=resources&op=readModal&item=" + id ,
            datatype: "json",
        })

            .done(function (data) {
                data = JSON.parse(data);
                // console.log(data);
                $('#idRes').html(data.IDResource);
                $('#typeRes').html(data.typeResource);
                $('#nameRes').html(data.nameResource);
                $('#editorialRes').html(data.editorialResource);
                $('#isbnRes').html(data.ISBNResource);
                $('#releaseRes').html(data.releaseResource);
                $('#genreRes').html(data.generoResource);
                $('#dateAddRes').html(data.dateAddedResource);


                $('#contentRes').removeAttr('hidden');

                $("#modalRes").dialog({
                    width: 850, 
                    height: 500, 
                    modal: "true",
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    },
                    show: {
                        effect: "slideDown",
                        duration: 200
                    },
                    hide: {
                        effect: "slideUp",
                        duration: 200
                    }
                })
            })

            .fail(function () {
                $('#contError').show();
                $('#modalError').dialog({
                    width: 300,
                    height: 150,
                    modal: "true",
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    },
                    show: {
                        effect: "slideDown",
                        duration: 200                        
                    },
                    hide: {
                        effect: "slideup",
                        duration: 200
                    }
                })
            })
        
    });
});

