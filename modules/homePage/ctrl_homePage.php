<?php
    class homePage {
    
        const PATH_MODEL = __DIR__ . '/model/model/';
        static $_instance;

        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        private function loadHead() {
            include VIEW_INC_PATH . '/headers/topPage.php';
            include VIEW_INC_PATH . '/header.php';
        }

        private function loadFooter() {
            include VIEW_INC_PATH . '/footer.php';
            include VIEW_INC_PATH . '/bottom.php';
        }

        public function loadView() {
            $this->loadHead();
            include 'view/index.html';
            $this->loadFooter();
        }

        public function search() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'homePage_model', 'get', 'search'));
            } catch (Exception $e) {
                echo $e->getMessage();
            }

        }

        public function autocomplete() {
            try {
                echo json_encode (loadModel(self::PATH_MODEL, 'homePage_model', 'get', 'autocomplete'));
            } catch (Exception $e) {
                echo $e->getMessage();
            }

        }

        public function getSuggestions() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'homePage_model', 'get', 'suggestions'));
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }


    }