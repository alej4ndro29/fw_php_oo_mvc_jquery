<?php
    $path_model = __DIR__.'/model/model/';
    $path = $_SERVER['DOCUMENT_ROOT'];
    include $path.'/utils/includes/includes.php';

    if (!isset($_GET['op'])) {
        include 'view/index.html';
    } else {
        // include 'dao/search.php';
        switch ($_GET['op']) {
            case 'search':
                try {
                    echo json_encode(loadModel($path_model, 'homePage_model', 'get', 'search'));
                } catch (Exception $e) {
                    echo $e->getMessage();
                }    
        	    break;

            case 'autocomplete':
                try {
                    echo json_encode (loadModel($path_model, 'homePage_model', 'get', 'autocomplete'));
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                break;
            
            case 'getSuggestions':
                try {
                    echo json_encode(loadModel($path_model, 'homePage_model', 'get', 'suggestions', $_GET['page']));
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                break;

            default:
                die('<script>window.location.href="./";</script>');
                break;
        }
    }