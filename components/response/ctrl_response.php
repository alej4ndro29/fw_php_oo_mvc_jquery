<?php
    class Response {
        static $_instance;
        
        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function ok($message = null) {
            $response['correct'] = true;
            
            if ($message != null) {
                $response['message'] = $message;
            }

            echo json_encode($response);

        }

        public function nok($message = null) {
            $response['correct'] = false;

            if ($message != null) {
                $response['message'] = $message;
            }

            echo json_encode($response);
        }


    }
