<?php
    class Db {
        private $server;
        private $user;
        private $passwd;
        private $database;
        private $sqli;
        private $stmt;
        private $array;

        static $_instance;

        private function __construct() {
            $this->getConnectionInfo();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }


        private function getConnectionInfo() {
            require_once 'Conf.class.singleton.php';
            $conf = Conf::getInstance();

            $this->user = $conf->_user;
            $this->passwd = $conf->_passwd;
            $this->server = $conf->_host;
            $this->database = $conf->_db;

        }

        private function connect() {
            $this->sqli = new mysqli($this->server, $this->user, $this->passwd);
            $this->sqli->select_db($this->database);
        }

        public function createQuery($sql) {
            $this->connect();
            $this->stmt = $this->sqli->query($sql);
            $this->disconnect();
            return $this->stmt;
        }

        public function listQuery($stmt) {
            $this->array = array();

            foreach ($stmt as $key) {
                array_push($this->array, $key);
            }
            
            return $this->array;
        }

        public function listOne($stmt) {
            return mysqli_fetch_assoc($stmt);
        }

        public function disconnect() {
            $this->sqli->close();
        }
        
    }
    