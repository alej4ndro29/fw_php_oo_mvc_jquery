<?php

	// if(isset($_GET['err503'])) {
	// 	include 'view/inc/err503.php';
	// }

	if (!isset($_GET['page'])) {
		include ('modules/homePage/ctrl_homePage.php');
	} else {
		if ($_GET['page']=="err503") {
			include 'view/inc/err503.php';
		} else if (file_exists("modules/" . $_GET['page'] . "/" . "ctrl_" . $_GET['page'] . ".php")){
			include "modules/" . $_GET['page'] . "/" . "ctrl_" . $_GET['page'] . ".php";
		} else {
			include 'view/inc/err404.php';
		}
	}
