<?php
    function sendMail($from, $to, $subject, $html) {
        $result = null;
        $curl = null;
        
        try {
            
            $curl = curl_init($_SESSION['MAILGUN_DOMAIN']);

            $mail = array(
                'from' => $from . ' <mailgun@'. $_SESSION['MAILGUN_DOMAIN_KEY'] .'>',
                'to' => $to,
                'subject' => $subject,
                'html' => $html
            );
        
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $_SESSION['MAILGUN_API_KEY']);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_POST, true); 
            curl_setopt($curl, CURLOPT_POSTFIELDS,$mail);
            
            $result = curl_exec($curl);
        } catch (Exception $e) {
            error_log('Error sending email');
        } finally {
            curl_close($curl);
        }

        return $result;

    }