<?php
    // OLD
    function connectDB() {
        $host = "localhost";
        $user = "root";
        $pass = "";
        $db = "biblioCafeTest";
        // if($_SESSION['testMode'])
        //     $db = "biblioCafeTest";
        // else
        //     $db = "biblioCafe";
        $port = "3306";
        
        $conexion = null;
        try {
            if (!$conexion = mysqli_connect($host, $user, $pass, $db, $port)) {
                throw new Exception();
            } 
        } catch(Exception $e) {
            header("Location: index.php?page=err503");
            die();
        }

        return $conexion;
    }

    function disconnectDB($conexion) {
        return mysqli_close($conexion);
    }