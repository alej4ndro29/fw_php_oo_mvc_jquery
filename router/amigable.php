<?php

    function amigable($dir){
        if (substr($dir, 0, 1) == '/') {
            return SITE_NAME . $dir;
        } else {
            return SITE_NAME . '/' . $dir;
        }
    }