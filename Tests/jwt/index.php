<?php
    require 'JWT.php';
    
    use \Firebase\JWT\JWT;

    $key = "exampleKey";
    $token = array(
        'user' => 'Your name',
        'time' => uniqid()
    );
    
    /**
     * IMPORTANT:
     * You must specify supported algorithms for your application. See
     * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
     * for a list of spec-compliant algorithms.
     */
    $a[0] = 0;
    $a[1] = 1;
    try {
        $jwt = JWT::encode($token, $key, 'HS256', 1,$a);
        $decoded = JWT::decode($jwt, $key, array('HS256'));
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    
    
    echo '<pre>';

    print_r($jwt);
    echo '<br>';
    print_r($decoded);

    echo '</pre>';
    