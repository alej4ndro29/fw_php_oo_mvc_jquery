<?php
    echo date('d/m/Y H:i:s');

    $datetime1 = new DateTime('now');
    
    $datetime2 = new DateTime('2019-03-03 10:00:00');

    // CALCULAR DIFERECIA ENTRE FECHAS
    $interval = $datetime1->diff($datetime2);

    // DIFERENCIA EN MINUTOS ENTRE DOS FECHAS
    $minutes = ($interval->days * 24 * 60) + ($interval->h * 60) + $interval->i;

    echo '<br>';
    echo $minutes;

    echo '<br>';
    echo date('12_d-m-Y_H:i:s');

    echo '<br>';
    $test = '12_'.date('d-m-Y_H:i:s');
    echo $test;