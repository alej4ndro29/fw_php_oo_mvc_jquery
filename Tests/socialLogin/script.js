var webAuth;
var userProfile;
var idToken;
var accessToken;
var expiresAt;

function handleAuthentication(callback) {
    webAuth.parseHash(function (err, authResult) {
        if (authResult && authResult.accessToken && authResult.idToken) {
            //setSession(authResult);
            console.log(authResult);
            $('#content').html(authResult);

            callback();
        } else {
            console.log(err);
        }
    });
}


function getProfile() {
    if (!userProfile) {
        var accessToken = localStorage.getItem('access_token');
        console.log('accestoken :' + accessToken);
        if (!accessToken) {
        } else {
            webAuth.client.userInfo(accessToken, function (err, profile) {
                console.log(profile);
            })
        }
    }
}

function logout() {
    // https://alej4-dev.eu.auth0.com/v2/logout

    // $.ajax({
    //     method: 'GET',
    //     url: 'modules/login/ctrl_login.php?op=register',
    //     datatype: 'json',
    //     data: data
    // });

    webAuth.logout({
        returnTo: 'http://localhost1/Tests/socialLogin/',
        client_id: 'HPa7flJZeZuk99N2ycB9jDC6AzCpTOlb'
    });
}


$(document).ready(function () {
    webAuth = new auth0.WebAuth({
        domain: 'alej4-dev.eu.auth0.com',
        clientID: 'HPa7flJZeZuk99N2ycB9jDC6AzCpTOlb',
        redirectUri: window.location.href,
        audience: 'https://' + 'alej4-dev.eu.auth0.com' + '/userinfo',
        responseType: 'token id_token',
        scope: 'openid profile',
        leeway: 60
    });


    $('#login_auth0').on('click', function (e) {
        // ej 9
        e.preventDefault();
        webAuth.authorize();
    });

    $('#logout_auth0').on('click', function () {
        logout();
    })

    handleAuthentication(getProfile);
    // setTimeout(function () { getProfile(); }, 1000);

});


